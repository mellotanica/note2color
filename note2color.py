#!/usr/bin/env python3

import matplotlib.cm
from PyQt5.QtWidgets import QWidget, QGridLayout, QLabel
from PyQt5 import QtCore
import math

class note2color:
    def __init__(self, scale_len, scale_base, colormap=matplotlib.cm.hsv):
        self.colormap = colormap
        self.scale_len = scale_len
        self.scale_base = scale_base

    def convert(self, note):
        return self.colormap(((note - self.scale_base) % self.scale_len) / self.scale_len)

class chord2color:
    def __init__(self, colormap=matplotlib.cm.hsv):
        self.n2c = note2color(12, 0, colormap)

    def convert(self, notes):
        if type(notes) is list or type(notes) is tuple:
            cols = []
            for n in notes:
                cols.append(self.n2c.convert(n % 12))
            return cols
        return self.n2c.convert(notes % 12)

class visualizer(QWidget):
    def __init__(self, converter, updater=None):
        super().__init__()

        self.converter = converter
        self.chord = type(self.converter) == chord2color
        self.notes = {}

        if updater is not None:
            updater.newnote.connect(self.add)
            updater.delnote.connect(self.remove)

            updater.start()

        self.layout = QGridLayout()
        self.setLayout(self.layout)

        self.show()

    def clear(self):
        for w in self.notes.values():
            self.layout.removeWidget(w[0])

    def rearrange(self):
        if len(self.notes) > 0:
            columns = math.ceil(math.log2(len(self.notes)))
            i = 0
            c = 0
            for w in self.notes.values():
                self.layout.addWidget(w[0], i, c)
                i += 1
                if i >= columns:
                    i = 0
                    c += 1

    @QtCore.pyqtSlot(int)
    def add(self, note):
        if self.chord:
            note = note % 12
        if note not in self.notes:
            col = self.converter.convert(note)
            rgb = (int(col[0] * 255), int(col[1] * 255), int(col[2] * 255))
            w = QLabel()
            w.setStyleSheet("QLabel {{ background-color: rgb({}, {}, {}); }}".format(rgb[0], rgb[1], rgb[2]))
            self.clear()
            self.notes[note] = [w, 1]
            self.rearrange()
            w.show()
        else:
            self.notes[note][1] += 1

    @QtCore.pyqtSlot(int)
    def remove(self, note):
        if self.chord:
            note = note % 12
        if note in self.notes:
            self.notes[note][1] -= 1
            if self.notes[note][1] == 0:
                self.clear()
                w = self.notes.pop(note)
                self.rearrange()
                w[0].deleteLater()


if __name__ == "__main__":
    import mido
    import argparse
    import time
    import sys
    from PyQt5.QtWidgets import QApplication, QMainWindow

    class updater(QtCore.QThread):
        newnote = QtCore.pyqtSignal(int)
        delnote = QtCore.pyqtSignal(int)
        endnotes = QtCore.pyqtSignal()

        def __init__(self, midiin, dosleep):
            super().__init__()
            self.midiin = midiin
            self.dosleep = dosleep

        def run(self):
            for msg in self.midiin:
                if self.dosleep:
                    time.sleep(msg.time)

                if msg.type is 'note_on':
                    self.newnote.emit(msg.note)
                elif msg.type is 'note_off':
                    self.delnote.emit(msg.note)
            self.endnotes.emit()

    parser = argparse.ArgumentParser()
    inputg = parser.add_mutually_exclusive_group(required=True)
    inputg.add_argument("-m", "--midi", help="midi port for real time processing")
    inputg.add_argument("-f", "--file", help="read midi file")
    inputg.add_argument("-l", "--list-midi", help="list available midi input ports and exit", action="store_true")

    modeg = parser.add_mutually_exclusive_group()
    modeg.add_argument("-c", "--chord", help="chord mode (ignore octave)", action="store_true")
    scaleg = modeg.add_argument_group()
    scaleg.add_argument("-sl", "--scale-len", help="scale lenght in steps", type=int)
    scaleg.add_argument("-sb", "--scale-base", help="scale base note", type=int)

    cmg = parser.add_mutually_exclusive_group()
    cmg.add_argument("-hsv", help="use hsv color map", action="store_true")
    cmg.add_argument("-rnb", help="use gist_rainbow color map", action="store_true")
    cmg.add_argument("-ncar", help="use gist_ncar color map", action="store_true")
    cmg.add_argument("-jet", help="use jet color map", action="store_true")

    args = parser.parse_args()

    app = QApplication(sys.argv)
    mw = QMainWindow()

    if args.list_midi:
        print("available midi input ports:")
        for p in mido.get_input_names():
            print(p)
        sys.exit(0)

    cm = matplotlib.cm.hsv
    mw.setWindowTitle("note2color: hsv")
    if args.rnb:
        cm = matplotlib.cm.gist_rainbow
        mw.setWindowTitle("note2color: gist_rainbow")
    elif args.ncar:
        cm = matplotlib.cm.gist_ncar
        mw.setWindowTitle("note2color: gist_ncar")
    elif args.jet:
        cm = matplotlib.cm.jet
        mw.setWindowTitle("note2color: jet")

    conv = None
    if args.chord:
        conv = chord2color(cm)
    else:
        sb = 0
        sl = 127
        if args.scale_base is not None:
            sb = args.scale_base
        if args.scale_len is not None:
            sl = args.scale_len
        conv = note2color(sl, sb, cm)

    midiin = None
    if args.midi is not None:
        midiin = mido.open_input(args.midi)
    else:
        midiin = mido.MidiFile(args.file)


    udth = updater(midiin, args.midi is None)

    vis = visualizer(conv, udth)

    mw.resize(800, 600)
    mw.setCentralWidget(vis)
    mw.show()

    sys.exit(app.exec_())


